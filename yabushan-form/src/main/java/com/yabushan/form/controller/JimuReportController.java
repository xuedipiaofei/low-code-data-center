package com.yabushan.form.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.form.domain.JimuReport;
import com.yabushan.form.service.IJimuReportService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 在线excel设计器Controller
 *
 * @author yabushan
 * @date 2021-07-03
 */
@RestController
@RequestMapping("/form/report")
public class JimuReportController extends BaseController
{
    @Autowired
    private IJimuReportService jimuReportService;

    /**
     * 查询在线excel设计器列表
     */
    @PreAuthorize("@ss.hasPermi('form:report:list')")
    @GetMapping("/list")
    public TableDataInfo list(JimuReport jimuReport)
    {
        startPage();
        List<JimuReport> list = jimuReportService.selectJimuReportList(jimuReport);
        return getDataTable(list);
    }

    /**
     * 导出在线excel设计器列表
     */
    @PreAuthorize("@ss.hasPermi('form:report:export')")
    @Log(title = "在线excel设计器", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(JimuReport jimuReport)
    {
        List<JimuReport> list = jimuReportService.selectJimuReportList(jimuReport);
        ExcelUtil<JimuReport> util = new ExcelUtil<JimuReport>(JimuReport.class);
        return util.exportExcel(list, "report");
    }

    /**
     * 获取在线excel设计器详细信息
     */
    @PreAuthorize("@ss.hasPermi('form:report:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(jimuReportService.selectJimuReportById(id));
    }

    /**
     * 新增在线excel设计器
     */
    @PreAuthorize("@ss.hasPermi('form:report:add')")
    @Log(title = "在线excel设计器", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody JimuReport jimuReport)
    {
        return toAjax(jimuReportService.insertJimuReport(jimuReport));
    }

    /**
     * 修改在线excel设计器
     */
    @PreAuthorize("@ss.hasPermi('form:report:edit')")
    @Log(title = "在线excel设计器", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody JimuReport jimuReport)
    {
        return toAjax(jimuReportService.updateJimuReport(jimuReport));
    }

    /**
     * 删除在线excel设计器
     */
    @PreAuthorize("@ss.hasPermi('form:report:remove')")
    @Log(title = "在线excel设计器", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(jimuReportService.deleteJimuReportByIds(ids));
    }
}
