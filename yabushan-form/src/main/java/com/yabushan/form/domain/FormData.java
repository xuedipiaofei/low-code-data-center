package com.yabushan.form.domain;

/**
 * @Author yabushan
 * @Date 2021/6/30 9:59
 * @Version 1.0
 */
public class FormData {

    private static final long serialVersionUID = 1L;

    private String formId;
    private String uid;
    private String[] key;
    private String[] value;


    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String[] getKey() {
        return key;
    }

    public void setKey(String[] key) {
        this.key = key;
    }

    public String[] getValue() {
        return value;
    }

    public void setValue(String[] value) {
        this.value = value;
    }
}
