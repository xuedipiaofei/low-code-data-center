package com.yabushan.form.domain;

/**
 * @Author yabushan
 * @Date 2021/6/30 15:00
 * @Version 1.0
 */
public class FormConstant {

    public  static final String createTime = "create_time";
    public static final String tableCreateTime = "create_time timestamp not null default current_timestamp";

    public static final Integer MAX_CUSTOM_CREATE_TABLE =5;//最多允许单个用户创建5个表
}
